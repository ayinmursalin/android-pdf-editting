package com.creativijaya.pdfdraw;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.pdf.PdfRenderer;
import android.os.ParcelFileDescriptor;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.appcompat.widget.AppCompatImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyPdfView extends AppCompatImageView {

    ArrayList<MyPath> touchPath = new ArrayList<>();

    // These matrices will be used to move and zoom image
    private Matrix matrix = new Matrix();
    private Matrix savedMatrix = new Matrix();

    // We can be in one of these 3 states
    public static final int NONE = 0;
    public static final int DRAG = 1;
    public static final int ZOOM = 2;
    public static final int WRITE = 3;
    private int mode = NONE;

    // Remember some things for zooming
    private PointF start = new PointF();
    private PointF mid = new PointF();
    private float oldDist = 1f;

    private final Paint paintLine = new Paint();
    private PdfRenderer pdfRenderer;
    private PdfRenderer.Page currentPdfPage;

    private int maxPage = 1;
    private int index = 0;
    private int currentPagePosition = 0;

    private ArrayList<Bitmap> bitmapList = new ArrayList<>();
    private ArrayList<Canvas> canvasList = new ArrayList<>();

    private File pdfFile;
    private MyPdfListener myPdfListener;

    /**
     * Construct the initial view
     *
     * @param context context where is inflated
     * @param set     attribute
     */
    public MyPdfView(Context context, AttributeSet set) {
        super(context, set);

        paintLine.setColor(Color.GREEN);
        paintLine.setAntiAlias(true);
        paintLine.setStyle(Paint.Style.STROKE);
        paintLine.setStrokeWidth(4);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (currentPagePosition >= 1) {
            Canvas customCanvas = canvasList.get(currentPagePosition - 1);

            for (MyPath path : touchPath) {
                if (path.getPage() == currentPagePosition) {
                    customCanvas.drawPath(path.getPath(), paintLine);
                }
            }
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);

        if (currentPagePosition >= 1) {
            Bitmap bitmap = bitmapList.get(currentPagePosition - 1);
            Drawable d = new BitmapDrawable(getResources(), bitmap);
            float imageWidth = d.getIntrinsicWidth();
            float imageHeight = d.getIntrinsicHeight();
            RectF drawableRect = new RectF(0, 0, imageWidth, imageHeight);
            RectF viewRect = new RectF(0, 0, getWidth(), getHeight());
            Matrix fullScreenMatrix = new Matrix();
            fullScreenMatrix.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);

            matrix.set(fullScreenMatrix);

            setImageMatrix(fullScreenMatrix);
            invalidate();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mode == WRITE) {
            float x = getPointerCoords(event)[0];
            float y = getPointerCoords(event)[1];

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touchPath.add(index, new MyPath(currentPagePosition, new Path()));
                    touchPath.get(index).getPath().moveTo(x, y);
                    break;
                case MotionEvent.ACTION_MOVE:
                    touchPath.get(index).getPath().lineTo(x, y);
                    break;
                case MotionEvent.ACTION_UP:
                    touchPath.get(index).getPath().lineTo(x, y);
                    index++;
                    break;
                default:
                    return false;
            }

            invalidate();
            return true;
        }

        dumpEvent(event);

        // Handle touch events here...
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
                mode = DRAG;
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                oldDist = spacing(event);
                if (oldDist > 10f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event);
                    mode = ZOOM;
                }
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                mode = NONE;
                break;
            case MotionEvent.ACTION_MOVE:
                if (mode == DRAG) {
                    // ...
                    matrix.set(savedMatrix);
                    matrix.postTranslate(event.getX() - start.x, event.getY()
                            - start.y);
                } else if (mode == ZOOM) {
                    float newDist = spacing(event);
                    if (newDist > 10f) {
                        matrix.set(savedMatrix);
                        float scale = newDist / oldDist;
                        matrix.postScale(scale, scale, mid.x, mid.y);
                    }
                }
                break;
        }

        setImageMatrix(matrix);
        invalidate();
        return true;
    }

    final float[] getPointerCoords(MotionEvent e) {
        final int index = e.getActionIndex();
        final float[] coords = new float[]{e.getX(index), e.getY(index)};
        Matrix matrix = new Matrix();
        getImageMatrix().invert(matrix);
        matrix.postTranslate(getScrollX(), getScrollY());
        matrix.mapPoints(coords);
        return coords;
    }

    private void dumpEvent(MotionEvent event) {
        String[] names = {"DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE",
                "POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?"};
        StringBuilder sb = new StringBuilder();
        int action = event.getAction();
        int actionCode = action & MotionEvent.ACTION_MASK;
        sb.append("event ACTION_").append(names[actionCode]);
        if (actionCode == MotionEvent.ACTION_POINTER_DOWN
                || actionCode == MotionEvent.ACTION_POINTER_UP) {
            //noinspection deprecation
            sb.append("(pid ").append(
                    action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
            sb.append(")");
        }
        sb.append("[");
        for (int i = 0; i < event.getPointerCount(); i++) {
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i + 1 < event.getPointerCount())
                sb.append(";");
        }
        sb.append("]");
    }

    /**
     * Determine the space between the first two fingers
     */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt((double) x * x + y * y);
    }

    /**
     * Calculate the mid point of the first two fingers
     */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    public void setPdfFile(File pdfFile, MyPdfListener listener) {
        this.pdfFile = pdfFile;
        this.myPdfListener = listener;

        setUpPdf();

        myPdfListener.onPdfLoaded(maxPage);
    }

    /**
     * Set up pdf file
     */
    private void setUpPdf() {
        try {
            pdfRenderer = new PdfRenderer(ParcelFileDescriptor.open(pdfFile,
                    ParcelFileDescriptor.MODE_READ_WRITE));

            currentPagePosition = 1;
            maxPage = pdfRenderer.getPageCount();

            allocateBitmaps();
            updatePage();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creating bitmap and canvas list
     */
    private void allocateBitmaps() {
        bitmapList.clear();
        canvasList.clear();

        for (int i = 0; i < maxPage; i++) {
            if (currentPdfPage != null) {
                currentPdfPage.close();
            }

            currentPdfPage = pdfRenderer.openPage(i);

            Bitmap bitmap = Bitmap.createBitmap(currentPdfPage.getWidth(),
                    currentPdfPage.getHeight(),
                    Bitmap.Config.ARGB_8888);

            currentPdfPage.render(bitmap, null, null,
                    PdfRenderer.Page.RENDER_MODE_FOR_PRINT);

            bitmapList.add(bitmap);
            canvasList.add(new Canvas(bitmap));
        }
    }

    public void onPrevPage() {
        if ((currentPagePosition - 1) < 1) {
            return;
        }
        currentPagePosition--;
        updatePage();
    }

    public void onNextPage() {
        if ((currentPagePosition + 1) > maxPage) {
            return;
        }
        currentPagePosition++;
        updatePage();
    }

    private void updatePage() {
        myPdfListener.onPdfPageChange(currentPagePosition);

        setImageBitmap(bitmapList.get(currentPagePosition - 1));
        invalidate();
    }

    public void setMode(int mode) {
        this.mode = mode;
        invalidate();
    }

    public List<Bitmap> getBitmaps() {
        return bitmapList;
    }

    public void undo() {
        if (index - 1 < 0) {
            return;
        }

        touchPath.remove(--index);
        setUpPdf();
        invalidate();
    }

    interface MyPdfListener {
        void onPdfLoaded(int maxPage);

        void onPdfPageChange(int currentPage);
    }

}

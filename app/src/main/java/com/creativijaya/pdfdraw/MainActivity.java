package com.creativijaya.pdfdraw;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.krishna.fileloader.FileLoader;
import com.krishna.fileloader.listener.FileRequestListener;
import com.krishna.fileloader.pojo.FileResponse;
import com.krishna.fileloader.request.FileLoadRequest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MyPdfView.MyPdfListener {

    private static final int EXTERNAL_STORAGE = 3024;
    private static final int MODE_DRAW = 1;
    private static final int MODE_DRAG = 2;

    MyPdfView pdfView;
    TextView tvPageInfo;
    ProgressBar pbPdf;
    ImageButton btnZoom;
    ImageButton btnWrite;

    private int currentPage = 1;
    private int maxPage = 1;
    private int mode = MODE_DRAG;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pdfView = findViewById(R.id.pdfView);
        tvPageInfo = findViewById(R.id.tvPageInfo);
        pbPdf = findViewById(R.id.pbPdf);
        ImageButton btnPrev = findViewById(R.id.btnPrev);
        ImageButton btnNext = findViewById(R.id.btnNext);
        btnWrite = findViewById(R.id.btnWrite);
        btnZoom = findViewById(R.id.btnZoom);
        ImageButton btnUndo = findViewById(R.id.btnUndo);
        ImageButton btnCheck = findViewById(R.id.btnCheck);

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdfView.onPrevPage();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdfView.onNextPage();
            }
        });

        btnWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mode = MODE_DRAW;
                pdfView.setMode(MyPdfView.WRITE);

                updateSelectedButton();
            }
        });

        btnZoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mode = MODE_DRAG;
                pdfView.setMode(MyPdfView.NONE);

                updateSelectedButton();
            }
        });

        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStorageAccessGranted()) {
                    createPdf();
                } else {
                    askPermission();
                }
            }
        });

        btnUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdfView.undo();
            }
        });

        updateSelectedButton();

        FileLoader.with(this)
                .load("https://riset.hallo.palembang.go.id/tes.pdf")
                .fromDirectory("Downloads", FileLoader.DIR_INTERNAL)
                .asFile(new FileRequestListener<File>() {
                    @Override
                    public void onLoad(FileLoadRequest request, FileResponse<File> response) {
                        Log.d("DEBUG_MAIN", "Pdf loaded " + response.getFileSize());
                        pbPdf.setVisibility(View.GONE);
                        pdfView.setPdfFile(response.getBody(), MainActivity.this);
                    }

                    @Override
                    public void onError(FileLoadRequest request, Throwable t) {
                        pbPdf.setVisibility(View.GONE);
                        Log.d("DEBUG_MAIN", "ERROR : " + t);
                    }
                });
    }

    private void updateSelectedButton() {
        if (mode == MODE_DRAW) {
            btnZoom.setSelected(false);
            btnWrite.setSelected(true);
        } else if (mode == MODE_DRAG) {
            btnZoom.setSelected(true);
            btnWrite.setSelected(false);
        }
    }

    private void createPdf() {
        List<Bitmap> bitmapList = pdfView.getBitmaps();

        PdfDocument document = new PdfDocument();

        for (int i = 0; i < bitmapList.size(); i++) {
            Bitmap bitmap = bitmapList.get(i);

            PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo
                    .Builder(bitmap.getWidth(), bitmap.getHeight(), i + 1)
                    .create();
            PdfDocument.Page page = document.startPage(pageInfo);

            Canvas canvas = page.getCanvas();
            canvas.drawBitmap(bitmap, 0, 0, null);
            document.finishPage(page);
        }

        try {
            File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
            if (!dir.exists()) {
                //noinspection ResultOfMethodCallIgnored
                dir.mkdir();
            }
            File newPdf = new File(dir, "test.pdf");
            FileOutputStream outputStream = new FileOutputStream(newPdf);
            document.writeTo(outputStream);

            document.close();

            startActivity(TestActivity.getIntent(MainActivity.this));
        } catch (IOException e) {
            Log.d("DEBUG_MAIN", "Error : " + e.getMessage());
            e.printStackTrace();
        }
    }

    private boolean isStorageAccessGranted() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void askPermission() {
        String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        ActivityCompat.requestPermissions(this, permissions, EXTERNAL_STORAGE);
    }

    @Override
    public void onPdfLoaded(int maxPage) {
        this.maxPage = maxPage;

        tvPageInfo.setText(getPageFormat());
    }

    @Override
    public void onPdfPageChange(int currentPage) {
        this.currentPage = currentPage;

        tvPageInfo.setText(getPageFormat());
    }

    private String getPageFormat() {
        return currentPage + " / " + maxPage;
    }

}

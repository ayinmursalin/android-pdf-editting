package com.creativijaya.pdfdraw;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.widget.ImageView;

import java.io.File;
import java.util.ArrayList;

public class TestActivity extends AppCompatActivity implements MyPdfView.MyPdfListener {

    public static Intent getIntent(Context context) {
        return new Intent(context, TestActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        final MyPdfView testPdfView = findViewById(R.id.testPdfView);

        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
        File savedPdf = new File(dir, "test.pdf");

        testPdfView.setPdfFile(savedPdf, this);
        testPdfView.setMode(MyPdfView.NONE);
    }

    @Override
    public void onPdfLoaded(int maxPage) {

    }

    @Override
    public void onPdfPageChange(int currentPage) {

    }
}

package com.creativijaya.pdfdraw;

import android.graphics.Path;

public class MyPath {

    private int page;
    private Path path;
    private boolean isErased;

    public MyPath(int page, Path path) {
        this.page = page;
        this.path = path;
        this.isErased = false;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public boolean isErased() {
        return isErased;
    }

    public void setErased(boolean erased) {
        isErased = erased;
    }
}
